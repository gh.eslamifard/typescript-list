export interface Todo {
    item: string,
    id: number,
    success: boolean
}

const list: Todo[] = [
    {
        item: "first",
        id: 3,
        success: false,
    },
    {
        item: "second",
        id: 12,
        success: false,
    },
    {
        item: "third",
        id: 15,
        success: false,
    },
    {
        item: "fourth",
        id: 8,
        success: true,
    },
];
export default list;