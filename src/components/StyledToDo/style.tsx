import styled from "@emotion/styled";

interface  Props {
   success: boolean;
}
export const Box = styled.div`
	padding: 10px;
	width:520px;
	height: auto;
	margin:auto;
	display:flex;
	flex-direction: column;
	
`;

export const UnorderedList = styled.ul`
	padding: 0;
	list-style: none;
	
`;

export const Wrapper = styled.li`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items:center;
	width:500px;
	height: 50px;
	direction: ltr;
	img {
		width: 20px;
		height:20px
	}
`;

export const Div = styled.div<Props>`
  background-color: ${({success})=>(success)?'#98ed9b':'#e8e8e8'};
  width: 435px;
  height: 40px;
  box-shadow: 2px 2px 3px 0 #d9d9d9;
  border-radius: 5px;
`;

export const Input = styled.input`
  background-color: white;
  width: 380px;
  height: 40px;
  box-shadow: 2px 2px 3px 0 #d9d9d9;
  border-radius: 5px;
`;
export const Button = styled.button`
border: none;
height: 40px;
width: 100px;
border-radius: 5px;
font-size: 16px;
`;