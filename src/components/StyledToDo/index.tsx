import React from "react";
import {useState} from 'react';
import {Box, Wrapper, Div, Button, Input, UnorderedList} from "./style";
import PencilIcon from '../../images/pencil.svg';
import trash from '../../images/trash.svg';
import list from '../../mock';

export default function StyleTOdO() {

    // inputValue keeps value of input
    const [inputValue, setInputValue] = useState("");
    // toDoList keeps all lists
    const [toDoList, setToDoList] = useState(list);
    // edit= true (edit item button) edit=false (add item)
    const [edit, setEdit] = useState(-1);
    const [count, setCount] = useState(46);
    const [filter, setFilter] = useState('all');
    const addItem = () => {
        setToDoList([...toDoList, {item: inputValue, id: count, success: false}]);
        setCount(count + 1);
    };

    //newList is an array without the element we wanted to remove
    const deleteItem = (id: number) => {
        const newList = toDoList.filter(value => {
            return value.id !== id;
        });
        setToDoList(newList);
    };
    const doneItem = (id:number) => {
        setToDoList(toDoList.map(todo => (todo.id === id ? {...todo, success:!todo.success} : todo)));
    };
    const editItem = () => {
        setToDoList(toDoList.map(todo => (todo.id === edit ? {...todo, item: inputValue} : todo)));
        setEdit(-1);
        setInputValue("");
    };
    const pencil = (id: number, item: string) => {
        setEdit(id);
        setInputValue(item);
    };

    const renderItems = () => {
        return toDoList.filter(item => {
            switch (filter) {
                case 'done':
                    return item.success;
                case 'ongoing':
                    return !item.success;
                default:
                    return true;
            }
        })
            .map(element => {
                return (<Wrapper key={element.id}>
                        <Div
                            success={element.success}
                            onClick={()=>doneItem(element.id)}
                        >{element.item}</Div>
                        <img
                            onClick={(): void => {
                                pencil(element.id, element.item)
                            }}
                            src={PencilIcon} alt='pencil'/>
                        <img
                            onClick={() => {
                                deleteItem(element.id)
                            }}
                            src={trash} alt='trash'/>
                    </Wrapper>
                )
            })
    };

    return (
        <Box>
            <Wrapper>
                <Button

                    onClick={() => {
                        setFilter('ongoing')
                    }}
                >ناموفق</Button>
                <Button
                    onClick={() => {
                        setFilter('done')
                    }}
                >موفق</Button>
                <Button
                    onClick={() => {
                        setFilter('all')
                    }}
                >نمایش همه</Button>
            </Wrapper>

            <UnorderedList>{renderItems()}</UnorderedList>
            <Wrapper>
                <Input
                    value={inputValue}
                    onChange={event => {
                        setInputValue(event.target.value);
                    }}
                />
                {
                    edit === -1 ? (
                            <Button
                                onClick={addItem}
                            > اضافه کردن</Button>
                        ) :
                        (
                            <Button
                                onClick={editItem}
                            >
                                ویرایش
                            </Button>
                        )
                }
            </Wrapper>
        </Box>
    )
}