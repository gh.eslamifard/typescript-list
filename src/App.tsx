import React, { Component } from 'react';
import './App.css';
import ToDo from "./components/StyledToDo";
class App extends Component {
  render() {
    return (
      <div className="App">
        <ToDo/>
      </div>
    );
  }
}

export default App;
